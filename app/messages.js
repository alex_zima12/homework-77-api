const express = require('express');
const router = express.Router();
const multer = require("multer");
const config = require("../config");
const fs = require('fs');
const {nanoid} = require("nanoid");
const path = require("path");

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get('/', (req, res) => {
    const dataList = JSON.parse(fs.readFileSync("./messages/db.json"));
    res.send(dataList);
});

router.post("/", upload.single("image"), async (req, res) => {
    const dataList = JSON.parse(fs.readFileSync("./messages/db.json"));
    let message = {...req.body};

    if (!req.body.message) {
        return res.status(404).send({error: "Message must be present in the request"})
    } else {
        if (!req.body.author){
            const author = "Anonymous";
            message = {...req.body, author};
        }
        if (req.file) {
            message.image = req.file.filename;
        }
        dataList.push(message)

        const data = JSON.stringify(dataList);
        await fs.writeFile('./messages/db.json', data, (err) => {
            if (err) throw err;
            console.log('The file has been saved!');
        });
    }
    res.send('File was saved!');
});

module.exports = router;

